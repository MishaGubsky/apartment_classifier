# Rent apartment classifier
### This classifier is written especially for @rent_apartment_bot telegram bot

## Installation
1. copy the directory: `git clone git@bitbucket.org:MishaGubsky/apartment_classifier.git`
2. install python 3.8:
- `sudo apt install software-properties-common`
- `sudo add-apt-repository ppa:deadsnakes/ppa`
- `sudo apt update`
- `sudo apt install python3.8`
3. install requirements: `pip3 install -r requirements.txt`

## Run Server
`python3 server.py`
