from tensorflow.keras import Model
from tensorflow.keras.utils import get_file
from tensorflow.keras.layers import GlobalAveragePooling2D, BatchNormalization, Activation, Dense, Dropout
from tensorflow.keras.preprocessing import image
import numpy as np
import cv2
import efficientnet.tfkeras as efn
import io
import os
from urllib.request import urlopen


class Classifier:
    IMAGE_SIZE = 320
    WEIGHTS_FILE_NAME = 'cian_model_01_B0.h5'
    WEIGHTS_PATH = 'https://fancyshot.com/wp-content/uploads/model/cian_model_01_B0.h5'

    def __init__(self):
        self.model = self._build_model()

    def _build_model(self) -> Model:
        pretrained_model = efn.EfficientNetB0(weights='imagenet', include_top=False)
        pretrained_model.trainable = False
        x = pretrained_model.output
        x = GlobalAveragePooling2D()(x)
        x = Dense(512, activation='relu')(x)
        x = BatchNormalization()(x)
        x = Activation('relu')(x)
        x = Dropout(0.5)(x)
        predictions = Dense(2, activation='softmax')(x)
        model = Model(inputs=pretrained_model.input, outputs=predictions)
        
        # lr=1e-4
        model.compile(loss='categorical_crossentropy',
                optimizer='adam', 
                metrics=['accuracy'])


        weights_path = get_file(fname=self.WEIGHTS_FILE_NAME, origin=self.WEIGHTS_PATH)
        model.load_weights(weights_path)
        return model
    
    def _load_image(self, url:str) -> np.ndarray:
        resp = urlopen(url)
        image = np.asarray(bytearray(resp.read()), dtype="uint8")
        image = cv2.imdecode(image, cv2.IMREAD_COLOR)
        return image
    
    def _prepare_image(self, image_array: np.ndarray) -> np.ndarray:
        image_array = cv2.cvtColor(image_array, cv2.COLOR_BGR2RGB)
        image_array = cv2.resize(image_array, (self.IMAGE_SIZE, self.IMAGE_SIZE))
        image_array = image.img_to_array(image_array) / 255.0
        image_array = np.expand_dims(image_array, axis=0)
        return image_array
    
    def _make_prediction(self, image) -> int:
        result = self.model.predict(image, batch_size=1)
        return int(round(result[0][1] * 10, 0))

    def predict(self, url: str) -> int:
        image = self._load_image(url)
        image = self._prepare_image(image)
        return self._make_prediction(image)


model = Classifier()
