from flask import Flask
from flask import request

from classifier import model

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Rent Apartment Classifier greats you!'


@app.route('/predict/', methods=['POST'])
def predict():
    return {url: model.predict(url) for url in request.json.get('urls', [])}

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080)
